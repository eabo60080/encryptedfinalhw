#include <Encrypted.h>
#include <algorithm>
Logistic::Logistic(int N, int M, double x0, double mu) : Permutation(N), M_(M), x0_(x0), mu_(mu) {}
void Logistic::gen_permutation()
{
	double x = x0_;
	for (int i = 0; i < M_; i++)
	{
		x = iter(x);
	}
	std::vector<std::pair<double, int>> v;
	for (int i = 0; i < p_.size(); i++)
	{
		x = iter(x);
		v.emplace_back(x, i);
	}
	sort(v.begin(), v.end());
	for (int i = 0; i < p_.size(); i++)
	{
		p_[i] = v[i].second;
	}
}
double Logistic::iter(double xn)
{
	return mu_ * xn * (1 - xn);
}