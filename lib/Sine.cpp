#include <Encrypted.h>
#include <algorithm>
#include <cmath>
Sine::Sine(int N, int M, double x0, double a) : Permutation(N), M_(M), x0_(x0), a_(a) {}
void Sine::gen_permutation()
{
	double x = x0_;
	for (int i = 0; i < M_; i++)
	{
		x = iter(x);
	}
	std::vector<std::pair<double, int>> v;
	for (int i = 0; i < p_.size(); i++)
	{
		x = iter(x);
		v.emplace_back(x, i);
	}
	sort(v.begin(), v.end());
	for (int i = 0; i < p_.size(); i++)
	{
		p_[i] = v[i].second;
	}
}
double Sine::iter(double xn)
{
	return a_ / 4. * sin(3.14159265358979323846 * xn);
}