#include <Encrypted.h>
template <typename T>
T gcd(T a, T b)
{
	return b == 0 ? a : gcd(b, a % b);
}

Permutation::Permutation(int N) : p_(N) {}
int Permutation::operator[](int k) const
{
	return p_[k];
}

long long Permutation::calculate_cyc()
{
	std::vector<int> vis(p_.size());
	for (int i = 0; i < p_.size(); i++)
	{
		vis[i] = -1;
	}
	for (int i = 0; i < p_.size(); i++)
	{
		if (vis[i] == -1)
		{
			std::vector<int> tmp_cyc;
			for (int u = i; vis[u] == -1; u = p_[u])
			{
				tmp_cyc.push_back(u);
				vis[u] = 1;
			}
			cyc_.emplace_back(std::move(tmp_cyc));
		}
	}
	if (cyc_.size() == 0)
	{
		return 1;
	}
	long long lcm = cyc_[0].size();
	for (int i = 1; i < cyc_.size(); i++)
	{
		long long gc = gcd(lcm, (long long)cyc_[i].size());
		lcm *= cyc_[i].size()/gc;

	}
	return lcm;
}

std::vector<std::vector<int>> Permutation::cyc()
{
	return cyc_;
}