#include <Encrypted.h>
#include <algorithm>
Shuffle::Shuffle(int N) : Permutation(N){}
std::mt19937 Shuffle::linearRan(time(0));
void Shuffle::gen_permutation()
{
	for (int i = 0; i < p_.size(); i++)
	{
		p_[i] = i;
	}
	std::shuffle(p_.begin(), p_.end(), Shuffle::linearRan);
}
