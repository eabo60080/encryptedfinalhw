#include <vector>
#include <random>
class Permutation
{
public:
	Permutation(int N);
	int operator[](int k) const;
	virtual void gen_permutation() = 0;
	long long calculate_cyc();
	std::vector<std::vector<int>> cyc();

protected:
	std::vector<int> p_;
	std::vector<std::vector<int>> cyc_;
};

class Shuffle : public Permutation
{
public:
	Shuffle(int N);
	virtual void gen_permutation() override;

private:
	static std::mt19937 linearRan;
};

class Logistic : public Permutation
{
public:
	Logistic(int N, int M, double x0, double mu);
	virtual void gen_permutation() override;

private:
	double iter(double);

private:
	int M_;
	double x0_, mu_;
};
class Chebyshev : public Permutation
{
public:
	Chebyshev(int N, int M, double x0, int k);
	virtual void gen_permutation() override;

private:
	double iter(double);

private:
	int M_, k_;
	double x0_;
};
class Sine : public Permutation
{
public:
	Sine(int N, int M, double x0, double a);
	virtual void gen_permutation() override;

private:
	double iter(double);

private:
	int M_;
	double x0_,a_;
};