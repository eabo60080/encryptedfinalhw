#include <Encrypted.h>
#include <iostream>
#include <random>
using namespace std;

int main()
{
	int N = 1024;
	random_device sd;  // 生成random_device对象sd做种子
	mt19937 linearRan(sd());
	// 使用种子初始化linear_congruential_engine对象，为的是使用它来做我们下面随机分布的种子以输出多个随机分布.注意这里要使用()操作符，因为minst_rand()接受的是一个值（你用srand也是给出这样的一个值）
	uniform_real_distribution<double> rand2(0, 1);
	double sum = 0;
	int T		  = 1000000;
	for (int i = 0; i < T; i++)
	{
		Logistic L(N, 1000, rand2(linearRan), 3.9);
		//Shuffle L(N);
		L.gen_permutation();
		auto lcm = L.calculate_cyc();
		sum += lcm;

		// for (int i = 0; i < 10; i++)
		//{
		//	cout << L(i) << " ";
		// }
		// cout << endl;
		
		//cout << "cyc===::" << lcm << endl;
		
		//auto cyc = L.cyc();
		//for (int i = 0; i < cyc.size(); i++)
		//{
		//	auto cyc_i = cyc[i];
		//	cout << "size:" << cyc_i.size() << endl;
		//	for (int j = 0; j < cyc_i.size(); j++)
		//	{
		//		cout << cyc_i[j] << " ";
		//	}
		//	cout << endl;
		//}
		//system("pause");
	}
	cout << (double)sum / T;
}